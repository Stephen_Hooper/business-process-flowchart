package bpflow

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

const (
	NONE     = "NONE"
	COMPLETE = "COMPLETE" //
	INVALID  = "INVALID"
	REQUEST  = "REQUEST"
	APPROVE  = "APPROVE"
	REJECT   = "REJECT"
	NOCHANGE = "NO CHANGE"
)

type Flowchart struct {
	Domain          string
	CreationRequest string
	Stages          map[string]Stage
	Transitions     map[string]Transition
}

func NewFlow(domain, creationRequest string) Flowchart {
	var outFlow Flowchart
	outFlow.Domain = domain
	outFlow.CreationRequest = creationRequest
	outFlow.Stages = make(map[string]Stage)
	outFlow.Transitions = make(map[string]Transition)
	return outFlow
}

func (this *Flowchart) AddTransition(newTransitions ...Transition) {
	for _, trans := range newTransitions {
		if this.Transitions[trans.Key].Key == "" { //if there's a zero-valued Transition at that point, it's empty.
			trans.parentFlow = this
			this.Transitions[trans.Key] = trans
		}
	}
}

func (this *Flowchart) AddStage(newStages ...Stage) {
	for _, stage := range newStages {
		if this.Stages[stage.Key].Key == "" { //if there's a zero-valued Stage at that point, it's empty.
			stage.parentFlow = this
			this.Stages[stage.Key] = stage
		}
	}
}

func (this *Flowchart) checkFlowRequest(currentDomainStatus, pendingRequestStatus, requestName, action string, skipApproval bool, validations ValidationTable) (string, string) {
	//find Stage and request

	var request Transition

	if currentDomainStatus != "" {
		currentStage := this.Stages[currentDomainStatus] //MUST TEST to see reaction to passing in impossible status
		if !currentStage.isValidRequest(requestName) {
			fmt.Println("Denied for not coming from correct stage")
			return INVALID, NOCHANGE
		}
	} else {
		if requestName != this.CreationRequest {
			fmt.Println("Denied for not being creation request")
			return INVALID, NOCHANGE
		}
	}
	request = this.Transitions[requestName]

	return request.checkRequest(action, pendingRequestStatus, skipApproval, validations)
}

func (this Flowchart) String() string {
	b, _ := json.Marshal(this)
	return string(b)
}

func contains(arr []string, str string) bool {
	for _, elem := range arr {
		if elem == str {
			return true
		}
	}
	return false
}

func newFromJSON(jsonSnip []byte) Flowchart {
	//Must use construction method so that all fields with pointers are correctily initialized
	var dummyFlow Flowchart
	_ = json.Unmarshal(jsonSnip, &dummyFlow)

	outFlow := NewFlow(dummyFlow.Domain, dummyFlow.CreationRequest)
	for _, val := range dummyFlow.Stages {
		outFlow.AddStage(val)
	}
	for _, val := range dummyFlow.Transitions {
		outFlow.AddTransition(val)
	}

	return outFlow
}

//write a Flow object to the chain
func writeFlow(stub shim.ChaincodeStubInterface, flow Flowchart) pb.Response {

	//check if flow is valid
	//if, then, etc.

	key := fmt.Sprintf("%s-%s", "flowchart", flow.Domain)
	b := []byte(flow.String())
	err := stub.PutState(key, b)
	if err != nil {
		logger.Debug("Couldn't input data", err)
		return shim.Error("Couldn't input data")
	}
	logger.Debug("Added " + key + " successfully")
	return shim.Success(b)
}

func getFlow(stub shim.ChaincodeStubInterface, domain string) (Flowchart, error) {
	key := fmt.Sprintf("%s-%s", "flowchart", domain)
	var newFlow Flowchart

	b, err := stub.GetState(key)
	if err != nil {
		return newFlow, err
	}

	newFlow = newFromJSON(b)

	return newFlow, nil
}
