package bpflow

import (
	"encoding/json"
)

type Stage struct {
	Key              string
	ValidTransitions []string
	parentFlow       *Flowchart
}

func NewStage(key string) Stage {
	var newStage Stage
	newStage.Key = key

	return newStage
}

func (this *Stage) AddNewTransition(key string) {
	if !contains(this.ValidTransitions, key) {
		this.ValidTransitions = append(this.ValidTransitions, key)
	}
}

func (this Stage) isValidRequest(reqKey string) bool {
	if contains(this.ValidTransitions, reqKey) {
		if this.parentFlow.Transitions[reqKey].Key == reqKey {
			return true
		}
	}
	return false
}

func (this Stage) String() string {
	b, _ := json.Marshal(this)
	return string(b)
}
