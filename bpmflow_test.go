/*
Copyright IBM Corp. 2016 All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

		 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package bpflow

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"testing"
)

const (
	testFlowString = "{\"Domain\":\"LendingAgreements\",\"CreationRequest\":\"Create\",\"Stages\":{\"Active\":{\"Key\":\"Active\",\"ValidTransitions\":[\"AddLendingClient\",\"Modify\",\"RequestEarlyTermination\",\"RequestExtendMaturityDate\",\"RequestPartialTermination\"]},\"Created\":{\"Key\":\"Created\",\"ValidTransitions\":[\"AddLendingClient\",\"Modify\"]},\"Matured\":{\"Key\":\"Matured\",\"ValidTransitions\":null},\"Terminated\":{\"Key\":\"Terminated\",\"ValidTransitions\":null}},\"Transitions\":{\"AddLendingClient\":{\"Key\":\"AddLendingClient\",\"Notes\":\"For Adding lending clients, can go to created or active\",\"WantsApproval\":false,\"NextStage\":{\"IsSTP:false\":\"Created\",\"IsSTP:true\":\"Active\"}},\"Create\":{\"Key\":\"Create\",\"Notes\":\"initial request for lending agreements, can go to created or active\",\"WantsApproval\":false,\"NextStage\":{\"IsSTP:false\":\"Created\",\"IsSTP:true\":\"Active\"}},\"Modify\":{\"Key\":\"Modify\",\"Notes\":\"Can only be done on Active or Created, and goes back to those\",\"WantsApproval\":false,\"NextStage\":{\"IsSTP:false\":\"Created\",\"IsSTP:true\":\"Active\"}},\"RequestEarlyTermination\":{\"Key\":\"RequestEarlyTermination\",\"Notes\":\"Request, can go to Matured or Terminated\",\"WantsApproval\":true,\"NextStage\":{\"IsSpecial:false\":\"Matured\",\"IsSpecial:true\":\"Terminated\"}},\"RequestExtendMaturityDate\":{\"Key\":\"RequestExtendMaturityDate\",\"Notes\":\"Request, only to/from Active\",\"WantsApproval\":true,\"NextStage\":{\" \":\"Active\"}},\"RequestPartialTermination\":{\"Key\":\"RequestPartialTermination\",\"Notes\":\"Request, only from Active, goes to Active\",\"WantsApproval\":true,\"NextStage\":{\" \":\"Active\"}}}}"
)

type RequestChaincode struct {
}

// func main() {

// 	logger.Info("Business Process Management Chaincode Activated")
// 	// Check for errors. Print message if an error occurs
// 	err := shim.Start(new(RequestChaincode))
// 	if err != nil {
// 		fmt.Printf("Error starting BPM chaincode: %s", err)
// 		logger.Error("Error starting BPM chaincode: %s", err)
// 	}
// }

func (c *RequestChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	logger.SetLevel(shim.LogError)

	return shim.Success(nil)
}

func (c *RequestChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {

	//TO DO:
	//getRequestStatus for a domain item?
	//some sort of output for request logs, but only if the entities saved there match the requesting entity
	//Question: IF the domain chaincode decides to spit out an error after checking a request here (and never updates its data store),
	//will this chaincode still write the new logs with that request data? I belive it's atomic transaction but I want to make sure

	function, args := stub.GetFunctionAndParameters()
	if function == "init" {
		return c.Init(stub)
	} else if function == "importFlow" {
		return ImportFlow(stub, args[0])
	} else if function == "viewFlow" {
		return ViewFlow(stub, args[0])
	} else if function == "checkRequest" {
		return CheckRequest(stub, args)
	} else if function == "insertTestFlow" {
		insertTestFlow(stub)
		return shim.Success([]byte(""))
	}

	logger.Error("Invalid Requests Invoke Function: " + function)
	return shim.Error("Invalid Invoke Function: " + function)
}

func checkInit(t *testing.T, stub *shim.MockStub) {
	res := stub.MockInit("1", [][]byte{[]byte("init")})
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}
}

func checkState(t *testing.T, stub *shim.MockStub, name string, value string) {
	bytes := stub.State[name]
	if bytes == nil {
		fmt.Println("State", name, "failed to get value")
		t.FailNow()
	}
	if string(bytes) != value {
		fmt.Println("State value", name, "was not", value, "as expected")
		t.FailNow()
	}
}

func checkQuery(t *testing.T, stub *shim.MockStub, query string, name string, value string) {
	res := stub.MockInvoke("1", [][]byte{[]byte(query), []byte(name)})
	if res.Status != shim.OK {
		fmt.Println("Query", name, "failed", string(res.Message))
		t.Fail()
	}
	if res.Payload == nil {
		fmt.Println("Query", name, "failed to get value")
		t.Fail()
	}
	if string(res.Payload) != value {
		fmt.Println("Query value", name, "was ", string(res.Payload), "not", value, "as expected")
		t.Fail()
	}
}

func checkRequestTest(t *testing.T, stub *shim.MockStub, domain, docKey, requestName, action, skipApproval, validations, entity string, value string) {
	res := stub.MockInvoke("1", [][]byte{[]byte("checkRequest"), []byte(domain), []byte(docKey), []byte(requestName), []byte(action), []byte(skipApproval), []byte(validations), []byte(entity)})
	if res.Status != shim.OK {
		fmt.Println("Check Request failed", string(res.Message))
		t.Fail()
	}
	if res.Payload == nil {
		fmt.Println("Check Request failed to get value")
		t.Fail()
	}
	if string(res.Payload) != value {
		fmt.Println("Check Request was ", string(res.Payload), "not", value, "as expected")
		t.Fail()
	}
}

func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func Test_Init(t *testing.T) {
	scc := new(RequestChaincode)
	stub := shim.NewMockStub("bpmrequests", scc)

	checkInit(t, stub)

}

// func Test_Invoke(t *testing.T) {
// 	scc := new(RequestChaincode)
// 	stub := shim.NewMockStub("bpmrequests", scc)

// 	checkInit(t, stub)

// 	// Invoke A->B for 123
// 	checkInvoke(t, stub, [][]byte{[]byte("invoke"), []byte("A"), []byte("B"), []byte("123")})
// 	checkQuery(t, stub, "A", "444")
// 	checkQuery(t, stub, "B", "801")

// 	// Invoke B->A for 234
// 	checkInvoke(t, stub, [][]byte{[]byte("invoke"), []byte("B"), []byte("A"), []byte("234")})
// 	checkQuery(t, stub, "A", "678")
// 	checkQuery(t, stub, "B", "567")
// 	checkQuery(t, stub, "A", "678")
// 	checkQuery(t, stub, "B", "567")
// }

func Test_viewFlow(t *testing.T) {
	scc := new(RequestChaincode)
	stub := shim.NewMockStub("bpmrequests", scc)

	checkInit(t, stub)
	checkInvoke(t, stub, [][]byte{[]byte("insertTestFlow")})

	// Query B
	checkQuery(t, stub, "viewFlow", "LendingAgreements", testFlowString)
	checkState(t, stub, "flowchart-LendingAgreements", testFlowString)
}

func Test_importFlow(t *testing.T) {
	scc := new(RequestChaincode)
	stub := shim.NewMockStub("bpmrequests", scc)

	checkInit(t, stub)

	checkInvoke(t, stub, [][]byte{[]byte("importFlow"), []byte(testFlowString)})

	checkState(t, stub, "flowchart-LendingAgreements", testFlowString)

}

// func Test_checkRequest(t *testing.T) {
// 	scc := new(RequestChaincode)
// 	stub := shim.NewMockStub("bpmrequests", scc)

// 	checkInit(t, stub)

// 	checkInvoke(t, stub, [][]byte{[]byte("insertTestFlow")})

// 	//check if current status is kept on-chain (can't create twice!)
// 	checkRequestTest(t, stub, "LendingAgreements", "001", "Create", REQUEST, "false", "IsSTP:false", "Steve", "{\"newDomStatus\":\"Created\",\"newStatus\":\"COMPLETE\"}")
// 	checkRequestTest(t, stub, "LendingAgreements", "001", "Create", REQUEST, "false", "IsSTP:true", "Steve", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"INVALID\"}")

// }

func Test_lifecycle1(t *testing.T) {
	scc := new(RequestChaincode)
	stub := shim.NewMockStub("bpmrequests", scc)

	checkInit(t, stub)

	checkInvoke(t, stub, [][]byte{[]byte("insertTestFlow")})

	//all approvals
	checkRequestTest(t, stub, "LendingAgreements", "001", "Create", REQUEST, "false", "IsSTP:true", "Steve", "{\"newDomStatus\":\"Active\",\"newStatus\":\"COMPLETE\"}")
	checkRequestTest(t, stub, "LendingAgreements", "001", "RequestExtendMaturityDate", REQUEST, "false", "", "SteveReq", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"PENDING_RequestExtendMaturityDate\"}")
	checkRequestTest(t, stub, "LendingAgreements", "001", "RequestExtendMaturityDate", APPROVE, "false", "", "SteveApp", "{\"newDomStatus\":\"Active\",\"newStatus\":\"COMPLETE\"}")

	//check transitioning with no matching validations
	checkRequestTest(t, stub, "LendingAgreements", "001", "RequestPartialTermination", REQUEST, "true", "wrongVal:true", "Steve", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"INVALID\"}")

	checkRequestTest(t, stub, "LendingAgreements", "001", "RequestEarlyTermination", REQUEST, "false", "IsSpecial:true", "SteveReq", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"PENDING_RequestEarlyTermination\"}")
	checkRequestTest(t, stub, "LendingAgreements", "001", "RequestEarlyTermination", APPROVE, "false", "IsSpecial:true", "SteveApp", "{\"newDomStatus\":\"Terminated\",\"newStatus\":\"COMPLETE\"}")

	//check transitioning when current stage doesn't support it
	checkRequestTest(t, stub, "LendingAgreements", "001", "RequestPartialTermination", REQUEST, "true", "", "Steve", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"INVALID\"}")
}

func Test_lifecycle2(t *testing.T) {
	scc := new(RequestChaincode)
	stub := shim.NewMockStub("bpmrequests", scc)

	checkInit(t, stub)

	checkInvoke(t, stub, [][]byte{[]byte("insertTestFlow")})

	//check if creation is protected
	checkRequestTest(t, stub, "LendingAgreements", "002", "Modify", REQUEST, "false", "IsSTP:true", "Steve", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"INVALID\"}")
	checkRequestTest(t, stub, "LendingAgreements", "002", "Create", REQUEST, "false", "IsSTP:false", "Steve", "{\"newDomStatus\":\"Created\",\"newStatus\":\"COMPLETE\"}")
	checkRequestTest(t, stub, "LendingAgreements", "002", "Modify", REQUEST, "false", "IsSTP:true", "Steve", "{\"newDomStatus\":\"Active\",\"newStatus\":\"COMPLETE\"}")

	//try rejecting something
	checkRequestTest(t, stub, "LendingAgreements", "002", "RequestExtendMaturityDate", REQUEST, "false", "", "SteveReq", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"PENDING_RequestExtendMaturityDate\"}")
	checkRequestTest(t, stub, "LendingAgreements", "002", "RequestExtendMaturityDate", REJECT, "false", "", "SteveApp", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"COMPLETE\"}")

	//illegal approval/rejection with nothing pending
	checkRequestTest(t, stub, "LendingAgreements", "002", "RequestExtendMaturityDate", APPROVE, "false", "", "SteveApp", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"INVALID\"}")
	checkRequestTest(t, stub, "LendingAgreements", "002", "RequestExtendMaturityDate", REJECT, "false", "", "SteveRej", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"INVALID\"}")

	//illegal request/approval/rejection with something else pending
	checkRequestTest(t, stub, "LendingAgreements", "002", "RequestExtendMaturityDate", REQUEST, "false", "", "SteveReq", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"PENDING_RequestExtendMaturityDate\"}")
	checkRequestTest(t, stub, "LendingAgreements", "002", "RequestPartialTermination", REQUEST, "false", "", "SteveReq", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"INVALID\"}")
	checkRequestTest(t, stub, "LendingAgreements", "002", "RequestPartialTermination", APPROVE, "false", "", "SteveApp", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"INVALID\"}")
	checkRequestTest(t, stub, "LendingAgreements", "002", "RequestPartialTermination", REJECT, "false", "", "SteveRej", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"INVALID\"}")

	// checkRequestTest(t, stub, "LendingAgreements", "002", "RequestEarlyTermination", REQUEST, "false", "IsSpecial:true", "SteveReq", "{\"newDomStatus\":\"NO CHANGE\",\"newStatus\":\"PENDING_RequestEarlyTermination\"}")
}

func Test_lifecycle3(t *testing.T) {
	scc := new(RequestChaincode)
	stub := shim.NewMockStub("bpmrequests", scc)

	checkInit(t, stub)

	checkInvoke(t, stub, [][]byte{[]byte("insertTestFlow")})

	//check request that skips approval
	checkRequestTest(t, stub, "LendingAgreements", "003", "Create", REQUEST, "false", "IsSTP:true", "Steve", "{\"newDomStatus\":\"Active\",\"newStatus\":\"COMPLETE\"}")
	checkRequestTest(t, stub, "LendingAgreements", "003", "RequestEarlyTermination", REQUEST, "true", "IsSpecial:true", "SteveReq", "{\"newDomStatus\":\"Terminated\",\"newStatus\":\"COMPLETE\"}")

}
