package bpflow

import (
	"encoding/json"
	"fmt"
)

type Transition struct {
	Key           string
	Notes         string
	WantsApproval bool
	NextStage     map[string]string
	parentFlow    *Flowchart
}

func NewTransition(key string, wantsApproval bool, notes string) Transition {
	var newTrans Transition
	newTrans.Key = key
	newTrans.WantsApproval = wantsApproval
	newTrans.Notes = notes

	newTrans.NextStage = make(map[string]string)

	return newTrans
}

func (this *Transition) AddNewStage(validations ValidationTable, key string) {
	this.NextStage[validations.GetHash()] = key
}

func (this Transition) checkRequest(action, requestStatus string, skipApproval bool, validations ValidationTable) (string, string) {

	//compare the incoming validations to this's appropriate validation map
	pendingStatusStr := fmt.Sprintf("PENDING_%s", this.Key)
	newStatus := ""
	if this.NextStage[validations.GetHash()] != "" {
		switch action {
		case REQUEST:
			if (!this.WantsApproval || skipApproval) && requestStatus == "NONE" { //if this is a new request with nothing pending and skipping approval
				newStatus = COMPLETE
			} else if requestStatus != NONE { //if this is a request on top of a pending request
				fmt.Println("Denied for sending a request on top of a pending request")
				return INVALID, NOCHANGE
			} else { //if this is a fresh request with no pending, but approval is needed
				newStatus = pendingStatusStr

			}
		case APPROVE:
			if requestStatus == pendingStatusStr {
				newStatus = COMPLETE
			} else if requestStatus == NONE {
				fmt.Println("Denied for approving a request with nothing pending")
				return INVALID, NOCHANGE
			} else {
				fmt.Println("Denied for approving a pending request of the wrong type")
				return INVALID, NOCHANGE
			}
		case REJECT:
			if requestStatus == pendingStatusStr {
				newStatus = COMPLETE
				return newStatus, NOCHANGE
			} else if requestStatus == NONE {
				fmt.Println("Denied for rejecting a request with nothing pending")
				return INVALID, NOCHANGE
			} else {
				fmt.Println("Denied for rejecting a pending request of the wrong type")
				return INVALID, NOCHANGE
			}
		default:
			fmt.Println("Denied for not being a request, rejection, or approval")
			return INVALID, NOCHANGE
		}
	} else {
		fmt.Println("Denied for not matching any stages")
		return INVALID, NOCHANGE
	}
	//assuming our validations pass...

	//for when you've made a request, the domain status should be nochange, not the otherwise correct stage
	if newStatus == pendingStatusStr {
		return newStatus, NOCHANGE
	}
	newDomainStatus := this.NextStage[validations.GetHash()]
	return newStatus, newDomainStatus

}

func (this Transition) String() string {
	b, _ := json.Marshal(this)
	return string(b)
}
