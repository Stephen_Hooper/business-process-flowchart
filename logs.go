package bpflow

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"strconv"
)

//Structure for recording all previous requests and their final status, also used to record current pending request
type RequestLog struct {
	Domain           string `json:"Domain"`
	DocumentKey      string `json:"DocumentKey"`
	RequestName      string `json:"RequestName"`
	DomainStatus     string `json:"DomainStatus"`  //Status of the actual object/document we're trying to make changes to
	RequestStatus    string `json:"RequestStatus"` //Status of the request, i.e. PENDING, COMPLETE, etc
	Action           string `json:"Action"`
	RequestingEntity string `json:"RequestingEntity"` //Entities are only stored for record-keeping. They are not validated or compared
	RespondingEntity string `json:"RespondingEntity"` //The chaincode using this request manager is responsible for any entity validation
	Validations      string `json:"Validations"`
	LogNumber        string `json: "LogNumber"`
}

//write a request log object to the chain
func writeReqLog(stub shim.ChaincodeStubInterface, reqLog RequestLog) pb.Response {

	key := fmt.Sprintf("%s-%s-%s-%s", "requestLog", reqLog.Domain, reqLog.DocumentKey, reqLog.LogNumber)
	b := reqLog.toBytes()
	err := stub.PutState(key, b)
	if err != nil {
		logger.Debug("Couldn't input data", err)
		return shim.Error("Couldn't input data")
	}
	logger.Info("Added " + key + " successfully")
	// logger.Warning(string(reqLog.toBytes()))
	return shim.Success(b)
}

func writePendingRequest(stub shim.ChaincodeStubInterface, pendingStatus RequestLog) pb.Response {
	key := fmt.Sprintf("%s-%s-%s", "pendingRequest", pendingStatus.Domain, pendingStatus.DocumentKey)
	b := pendingStatus.toBytes()
	err := stub.PutState(key, b)
	if err != nil {
		logger.Debug("Couldn't input data", err)
		return shim.Error("Couldn't input data")
	}
	logger.Info("Added " + key + " successfully")
	// logger.Warning(string(pendingStatus.toBytes()))
	return shim.Success(b)

}

func getReqLog(stub shim.ChaincodeStubInterface, domain, docKey, logNum string) (RequestLog, pb.Response) {
	key := fmt.Sprintf("%s-%s-%s-%s", "requestLog", domain, docKey, logNum)
	var reqLog RequestLog

	b, err := stub.GetState(key)
	if err != nil {
		return reqLog, shim.Error(err.Error())
	}
	if len(b) == 0 {
		logger.Debug("Request Log not found for " + key)
		return reqLog, shim.Success([]byte(""))
	}

	err = reqLog.fromBytes(b)
	if err != nil {
		return reqLog, shim.Error(err.Error())
	}
	return reqLog, shim.Success([]byte(""))

}

func getPendingRequest(stub shim.ChaincodeStubInterface, domain, docKey string) (RequestLog, pb.Response) {
	key := fmt.Sprintf("%s-%s-%s", "pendingRequest", domain, docKey)
	var reqLog RequestLog
	logger.Debug("Looking up " + key)
	b, err := stub.GetState(key)
	if err != nil {
		return reqLog, shim.Error(err.Error())
	}
	if len(b) == 0 {
		logger.Debug("Pending Request not found for " + key)
		return reqLog, shim.Success([]byte(""))
	}

	err = reqLog.fromBytes(b)
	if err != nil {
		return reqLog, shim.Error(err.Error())
	}

	return reqLog, shim.Success([]byte(""))

}

func getStatuses(stub shim.ChaincodeStubInterface, domain, docKey string) (string, string, pb.Response) {
	reqLog, response := getPendingRequest(stub, domain, docKey)
	if response.Status != shim.OK {
		return "", "", response
	}

	return reqLog.DomainStatus, reqLog.RequestStatus, shim.Success([]byte(""))
}

func (this RequestLog) toBytes() []byte {
	b, _ := json.Marshal(this)
	return b
}

func (this *RequestLog) fromBytes(b []byte) error {
	return json.Unmarshal(b, &this)
}

func (this *RequestLog) increment() {
	aNum, err := strconv.Atoi(this.LogNumber)
	if err != nil {
		logger.Warning("Problem converting string to integer in RequestLog.increment()")
		logger.Debug(err)
	}
	aNum += 1
	this.LogNumber = strconv.Itoa(aNum)
}

func newRequestLog(domain, docKey string) RequestLog {
	var outLog RequestLog

	outLog.Domain = domain
	outLog.DocumentKey = docKey
	outLog.RequestStatus = "NONE"
	outLog.LogNumber = "0"

	return outLog
}

func (this RequestLog) split(requestName, newDomainStatus, newRequestStatus, action, entity, validations string) (RequestLog, RequestLog) {

	//make log
	var reqLog RequestLog
	reqLog.Domain = this.Domain
	reqLog.DocumentKey = this.DocumentKey
	reqLog.RequestName = requestName
	reqLog.DomainStatus = newDomainStatus
	reqLog.RequestStatus = newRequestStatus
	reqLog.Action = action
	if action == REQUEST {
		reqLog.RequestingEntity = entity
		reqLog.RespondingEntity = "N/A"
	} else {
		reqLog.RequestingEntity = this.RequestingEntity
		reqLog.RespondingEntity = entity
	}
	reqLog.Validations = validations
	reqLog.LogNumber = this.LogNumber

	//make pending

	if newDomainStatus == NOCHANGE {
		newDomainStatus = this.DomainStatus
	}

	var newPendingStatus RequestLog
	newPendingStatus.Domain = this.Domain
	newPendingStatus.DocumentKey = this.DocumentKey

	switch newRequestStatus {
	case COMPLETE: //if the request completed successfully or rejected on purpose
		newPendingStatus.RequestName = ""
		newPendingStatus.DomainStatus = newDomainStatus
		newPendingStatus.RequestStatus = NONE
		newPendingStatus.Action = ""
		newPendingStatus.RequestingEntity = ""
		newPendingStatus.RespondingEntity = ""
		newPendingStatus.Validations = ""
	case INVALID: //if the request was invalid
		newPendingStatus.RequestName = this.RequestName
		newPendingStatus.DomainStatus = this.DomainStatus
		newPendingStatus.RequestStatus = this.RequestStatus
		newPendingStatus.Action = ""
		newPendingStatus.RequestingEntity = this.RequestingEntity
		newPendingStatus.RespondingEntity = this.RespondingEntity
		newPendingStatus.Validations = this.Validations

		// if this.DomainStatus == "" { //a creation request that ended up being invalid needs special attention
		// 		newPendingStatus.RequestStatus = NONE

		// 	}
	default: //if the request is still pending
		newPendingStatus.RequestName = this.RequestName
		newPendingStatus.DomainStatus = newDomainStatus
		newPendingStatus.RequestStatus = newRequestStatus
		newPendingStatus.Action = ""
		newPendingStatus.RequestingEntity = reqLog.RequestingEntity
		newPendingStatus.RespondingEntity = reqLog.RespondingEntity
		newPendingStatus.Validations = validations
	}

	newPendingStatus.LogNumber = this.LogNumber

	return reqLog, newPendingStatus
}
