package bpflow

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"strconv"
)

var logger = shim.NewLogger("BPM Logger")

/**
ImportFlow() writes a flow to the blockchain.
The new flow is useable as soon as it is saved.
IN: the chaincode stub interface
IN: the JSON representation of a complete flow
OUT: a chaincode response
*/
func ImportFlow(stub shim.ChaincodeStubInterface, flowStr string) pb.Response {
	newFlow := newFromJSON([]byte(flowStr))
	return writeFlow(stub, newFlow)
}

/**
ViewFlow() returns an active flow if it exists
IN: the chaincode stub interface
IN: the domain of the flow to be queried
OUT: a chaincode response
	Payload of success response will be JSON representation of the flow
*/
func ViewFlow(stub shim.ChaincodeStubInterface, domain string) pb.Response {
	key := fmt.Sprintf("%s-%s", "flowchart", domain)
	b, err := stub.GetState(key)

	if err != nil {
		return shim.Error("Couldn't retrieve Flow")
	}

	return shim.Success(b)
}

/**
CheckRequest() runs an incoming request against the appropriate flow, to decide whether an action is allowed
and what the resulting status is.
All requests against an asset are saved to the logs for that particular asset, regardless of outcome.
Since all requests that "pass" will update that asset's log, be careful to maintain parity between these logs
and the actual on-chain asset
IN: the chaincode stub interface
IN: an array of strings containing the request info (see README)
OUT: a chaincode response
	Payload of success response will be a map containing the asset's suggested status and the status of the request
*/
func CheckRequest(stub shim.ChaincodeStubInterface, argList []string) pb.Response { //TODO split this args input into actual inputs
	if len(argList) != 7 {
		return shim.Error("Wrong number of arguments sent in")
	}
	domain := argList[0]
	docKey := argList[1]
	requestName := argList[2]
	action := argList[3]
	skipApproval, _ := strconv.ParseBool(argList[4])
	validations := NewValidationTable(argList[5])
	entity := argList[6]

	//get current domain status and check for pending requests

	pendingRequest, response := getPendingRequest(stub, domain, docKey)
	if response.Status != shim.OK {
		logger.Debug("Could not pull pending status from chaincode")
		logger.Debug(response.Message)
		return shim.Error("Could not pull pending status from chaincode: " + response.Message)
	}
	if pendingRequest.RequestStatus == "" { //this means there was no previous status, so we should initialize one now.
		pendingRequest = newRequestLog(domain, docKey)
	} else {
		pendingRequest.increment()
	}
	currentDomainStatus := pendingRequest.DomainStatus
	pendingRequestStatus := pendingRequest.RequestStatus

	//get flow
	flow, err := getFlow(stub, domain)
	if err != nil {
		return shim.Error("Couldn't retrieve Flow")
	}

	newStatus, newDomStatus := flow.checkFlowRequest(currentDomainStatus, pendingRequestStatus, requestName, action, skipApproval, validations)

	//Write current status and log to the chaincode
	reqLog, newPendingRequest := pendingRequest.split(requestName, newDomStatus, newStatus, action, entity, validations.GetHash())

	response = writeReqLog(stub, reqLog)
	if response.Status != shim.OK {
		logger.Debug("Trouble writing request log to chaincode")
		logger.Debug(response.Message)
		return shim.Error("Trouble writing request log to chaincode: " + response.Message)
	}

	response = writePendingRequest(stub, newPendingRequest)
	if response.Status != shim.OK {
		logger.Debug("Trouble writing pending request to chaincode")
		logger.Debug(response.Message)
		return shim.Error("Trouble writing pending request to chaincode: " + response.Message)
	}

	outMap := make(map[string]string)
	outMap["newStatus"] = newStatus
	outMap["newDomStatus"] = newDomStatus

	outbytes, _ := json.Marshal(outMap)

	return shim.Success(outbytes)

	// test.CheckRequest(currentDomainStatus, pendingRequestStatus, requestName, action, skipApproval, validations)
}

/**
insertTestFlow() is an internal function used for testing.
It can also exemplify the process of creating a flow from scratch
IN: the chaincode stub interface
OUT: a chaincode response
*/

func insertTestFlow(stub shim.ChaincodeStubInterface) {
	//initialize new flow with NewFlow(domain, creationRequest)
	//domain is the name of the type of asset this flow should control
	//creationRequest is the name of the transition that is allowed to make new assets. This is the beginning of the flow.
	test1 := NewFlow("LendingAgreements", "Create")

	//first transition with NewTransition(key, wantsApproval, notes)
	create := NewTransition("Create", false, "initial request for lending agreements, can go to created or active")

	//initialize a new validation table by using an empty string, then insert flags
	valCreated := NewValidationTable("")
	valCreated.InsertFlag("IsSTP", false)

	//even though the "Created" stage doesn't exist yet, we add a reference to it in our transition
	//using create.AddNewStage(validations, key)
	//each stage reachable from this transition will be referenced by its key and by a unique validation table
	create.AddNewStage(valCreated, "Created")

	//we can use valCreated as a template for another validation table valActive
	//we'll just change "IsSTP" from false to true
	valActive := valCreated.MakeCopy()
	valActive.InsertFlag("IsSTP", true)

	//After this, 'create' will be able to result in 'Created' or 'Active' based on the result of "IsSTP"
	create.AddNewStage(valActive, "Active")

	//next transition
	addLendingClient := NewTransition("AddLendingClient", false, "For Adding lending clients, can go to created or active")
	//we can recycle those validation tables
	addLendingClient.AddNewStage(valCreated, "Created")
	addLendingClient.AddNewStage(valActive, "Active")

	//next transition
	reqEarlyTerm := NewTransition("RequestEarlyTermination", true, "Request, can go to Matured or Terminated")

	valMatured := NewValidationTable("")
	valMatured.InsertFlag("IsSpecial", false)
	reqEarlyTerm.AddNewStage(valMatured, "Matured")

	valTerminated := valMatured.MakeCopy()
	valTerminated.InsertFlag("IsSpecial", true)
	reqEarlyTerm.AddNewStage(valTerminated, "Terminated")

	//next Transition
	modify := NewTransition("Modify", false, "Can only be done on Active or Created, and goes back to those")
	modify.AddNewStage(valCreated, "Created")
	modify.AddNewStage(valActive, "Active")

	//next transition
	//for this one, we don't need any special flags because there's only one possible outcome
	//so we will add a blank validation table for the Active stage
	reqExtendMatDate := NewTransition("RequestExtendMaturityDate", true, "Request, only to/from Active")
	reqExtendMatDate.AddNewStage(NewValidationTable(""), "Active")

	//next transition
	reqPartialTerm := NewTransition("RequestPartialTermination", true, "Request, only from Active, goes to Active")
	reqPartialTerm.AddNewStage(NewValidationTable(""), "Active")

	//first stage using NewStage(key)
	created := NewStage("Created")

	//In order to link a transition to this stage, we only need a reference to its key.
	//Make sure it matches the key used earlier.
	//A "Created" asset can be acted on by two transitions.
	created.AddNewTransition("AddLendingClient")
	created.AddNewTransition("Modify")

	//next stage
	//Since "Active" is our default status, it has the most possible transitions
	active := NewStage("Active")
	active.AddNewTransition("AddLendingClient")
	active.AddNewTransition("Modify")
	active.AddNewTransition("RequestEarlyTermination")
	active.AddNewTransition("RequestExtendMaturityDate")
	active.AddNewTransition("RequestPartialTermination")

	//next stage
	matured := NewStage("Matured")

	//next stage
	terminated := NewStage("Terminated")

	//Add all transitions and stages to our flow. This can be done one at a time or all at once.
	//ONLY use these setters, do not assign Stages or Transitions manually.

	test1.AddTransition(create, addLendingClient, reqEarlyTerm, modify, reqExtendMatDate, reqPartialTerm)
	test1.AddStage(created, active, matured, terminated)

	_ = writeFlow(stub, test1)
}
