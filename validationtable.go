package bpflow

import (
	"fmt"
	"strconv"
	"strings"
)

type ValidationTable struct {
	valMap map[string]bool
	keys   []string
}

func NewValidationTable(hash string) ValidationTable {
	var outTable ValidationTable
	outTable.valMap = make(map[string]bool)
	if hash == "" || hash == " " {
		return outTable
	}
	//else do some importing
	flagList := strings.Split(hash, ",")
	for _, oneFlag := range flagList {
		keyValPair := strings.Split(oneFlag, ":")
		flagValue, _ := strconv.ParseBool(keyValPair[1])
		outTable.InsertFlag(keyValPair[0], flagValue)
	}
	return outTable

}

func (this *ValidationTable) InsertFlag(flag string, value bool) {
	if !contains(this.keys, flag) {
		this.keys = append(this.keys, flag)
	}
	this.valMap[flag] = value
}

func (this ValidationTable) GetHash() string {
	var outStr []string
	if len(this.keys) == 0 {
		return " "
	}
	for _, key := range this.keys {
		strPart := fmt.Sprintf("%s:%t", key, this.valMap[key])
		outStr = append(outStr, strPart)
	}
	return strings.Join(outStr, ",")
}

func (this ValidationTable) MakeCopy() ValidationTable {
	thisHash := this.GetHash()
	return NewValidationTable(thisHash)
}
