# Business Process Flowchart
This is a package designed for utilization in Golang chaincodes running on Hyperledger's Fabric. If you want to port the logic to other frameworks or languages, email the author at stephen.e.hooper@ibm.com
## Background
In business cases, database assets can have complex lifecycles, based on arbitrary rules, statuses, and actions that can be taken. In some cases, that lifecycle might be subject to change as the business needs evolve. When the rules governing an asset's lifecycle are very complex or potentially dynamic, it can be helpful to separate the business logic from the rest of the database logic. This package allows a developer to make that separation, while also creating a framework that is easy to understand and adapt for most business needs. Further, the business logic for an asset is stored on-chain, meaning that it can be dynamically updated with little or no changes to the actual chaincode with a simple invocation. The JSON formatting is easy to read and could in the future be used to generate images for presentation or collaboration.

A lifecycle is modeled as a flowchart made of 'stages' (analogous to status) connected to each other by 'transitions'. Stages can connect to multiple transitions, and each transition supports multiple potential 'next' stages, based on any validation tests performed in the chaincode. This flexibilty allows for arbitrarily complex flowcharts with branches and loops. 

BPFlow automatically logs all attempted requests and stores the state of every asset on chain. Therefore, care must be taken to make sure that parity is kept between the BPFlow's record and the actual asset onchain. The state includes the Domain status (status of the asset), and a Request Status (used when a request is pending approval by a second party).




## Setup
Using this package with your chaincode is easy. Download or clone this repo to your local GOPATH under `gitlab.com/Stephen_Hooper/business-process-flowchart` and run `go install`. Then import it in your chaincode files as needed. 
### Creating a Flow
There are two ways to create a Flow object: from scratch, using the public functions outlined in `insertTestFlow()` from `chaincodeFunctions.go`, or with the JSON representation of a completed Flow. When creating a Flow from scratch, it's best to do it all at once. Since all Stages and Transitions need references to each other, it is not advised to add to an unknown Flow object. Editing or revising a Flow can be done directly to the JSON string itself. To enable a Flow and write it to the blockchain, use `ImportFlow()` with a JSON string. JSON can be generated from a Flow object with Flow.String() .
### Reading and Updating Flows
The current flow can be read with ViewFlow()

### Using the Flow
With BPFlow, to interact with an asset you submit a 'request' against that asset's flow. A request contains:
- A 'Domain', or asset type (in case there are different kinds of assets being handled in one chaincode)
- A key for the specific asset you intend to change
- The name of the transition you wish to invoke
- The action you wish to invoke (Request, Approve, or Reject)
- A boolean flag for skipping approval (as a concession to any special circumstances based on your business logic)
- A validation table, which is essentially a map of testname:boolean string. This allows a Transition to result in different stages based on any logic you might like in your chaincode. 
- A string representing the requesting entity. This will be saved in the logs.

The Flow checks the action you wish to take (and any extra validation you send in) against the current status of the asset and decides what the next status should be. Based on that new status, your chaincode can act accordingly. Once this is performed, BPFlow will update its records of the asset's status, so this should only be done concurrently with any asset changes.

Flowcharts also support approve/reject logic, meaning that one party can "request" a status change, and a second party can choose to reject or approve it before any change is enacted on the database asset. As mentioned above, when a request is pending approval that record is stored onchain and will be checked when the next request is checked. Keep in mind that a request pending for an asset will LOCK that asset until the pending request is either rejected or approved; no requests will pass on that asset.

In your chaincode function, use `bpflow.CheckRequest(stub, args)` where `args` is an array of strings with the above data. If `res.Status` is OK, then you can unmarshal the payload into a map. The map contains the `newStatus` and `newDomStatus` from your request. If `newDomStatus` is bpflow.NOCHANGE then no more action should be taken on the asset. *NOTE: Do not* return a shim.Error due to a `NOCHANGE` response. This will prevent the chaincode from logging the request. 

If the returned status is anything else, then your chaincode should make changes to the asset accordingly. 


### TO DO
- Consider changing output of CheckRequest so that it returns the status map directly instead of as a payload (along with the pb.Response)

- Add "withdraw" action for a party to withdraw their pending action and free up that asset

- Test for resiliency when the Flow is changed. Current asset stages and pending transactions should still be supported.

- Maybe add a mechanism to control who can approve/reject a pending transaction (UPDATE: Krishna's ACL chaincode helper would be good 
here)